require('co-mocha');
var should = require('should');
var data = require('../book-data.js');
var fs = require('co-fs');
var api = require('../book-api.js');
var request = require('co-supertest').agent(api.listen());

before(function *(){
  yield fs.writeFile('./books.json', '[]');
});

describe('user data', function() {
  it('should have +1 book count after saving', function* () {
    var books = yield data.books.get();

    yield data.books.save({title: 'Javascript: The good parts', author: 'Douglas Crockford'});

    var newBooks = yield data.books.get();

    newBooks.length.should.equal(books.length + 1);
  });
});

describe('book api', function() {
  it('should have +1 book count after saving', function* () {
    var books = (yield request.get('/books').expect(200).end()).body;

    yield data.books.save({title: 'Javascript: The good parts', author: 'Douglas Crockford'});

    var newBooks = (yield request.get('/books').expect(200).end()).body;

    newBooks.length.should.equal(books.length + 1);
  });
});