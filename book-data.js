var fs = require('co-fs');
var bookFile = './books.json';

module.exports = {
  books: {
    get: function* () {
      var data = yield fs.readFile(bookFile, 'utf8');
      return JSON.parse(data);
    },
    save: function* (user) {
      var books = yield this.get();
      books.push(user);
      yield fs.writeFile(bookFile, JSON.stringify(books));
    }
  }
};