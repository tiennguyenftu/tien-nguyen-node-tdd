var koa = require('koa');
var router = require('koa-router')();

var data = require('./book-data.js');

var app = module.exports = koa();


router.get('/books', function* (){
  this.body = yield data.books.get();
});

app.use(router.routes());

app.listen(3000);

